import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class CardCollectiblesService {

  data = [
    {
      Id: 1,
      Name: 'Jojo',
      Owner: 'John Tom',
      Price: 0.99,
      ImageUrl: './assets/images/sample1.jpeg',
      Display: true,
      isSold: false
    },
    {
      Id: 2,
      Name: 'Shia',
      Owner: 'Kriesha Connor',
      Price: 0.99,
      ImageUrl: './assets/images/sample2.jpeg',
      Display: true,
      isSold: false
    },
    {
      Id: 3,
      Name: 'Jane Doe',
      Owner: 'Mary Jane Yu',
      ImageUrl: './assets/images/sample3.jpeg',
      Price: 0,
      Display: true,
      isSold: true
    }
  ]

  cardCollectibles: any = [];
  status: string;

  constructor() { }

  loadCollectibles() {
    this.data.forEach(data => {
      this.cardCollectibles.push(data)
    })
    return this.cardCollectibles
  }

  addCard(item: any) {
    this.cardCollectibles.push(item)
    return this.cardCollectibles
  }

  setStatus(status: string) {
    return this.status = status;
  }

  getStatus() {
    return this.status
  }

  getCardList() {
    return this.cardCollectibles
  }
}
