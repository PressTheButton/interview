import { TestBed } from '@angular/core/testing';

import { CardCollectiblesService } from './card-collectibles.service';

describe('CardCollectiblesService', () => {
  let service: CardCollectiblesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CardCollectiblesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
