import { AfterViewInit, Component, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CardComponent } from '../card/card.component';
import { CardCollectiblesService } from '../services/card-collectibles.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {

  cards: Array<any> = [];
  sold: boolean;
  constructor(private cardService: CardCollectiblesService, private _snackbar: MatSnackBar) { 

  }

  // Get updated list of collectibles
  ngOnInit() {
    this.cards = this.cardService.getCardList()
  }

  // Set status for Filter and display applicable collectibles
  onStatus(status: string) {
    this.cardService.setStatus(status);
    
    this.cards = this.cardService.getCardList();

    this.checkCards();
  }

  buy(id: number) {
    this.cards.find(item => item.Id == id).Price = 0;
    this.cards.find(item => item.Id == id).isSold = true;

    this.openSnackbar('Congratulations, you just bought a collectible!');
  }

  openSnackbar(text: string) {
    const durationInSeconds = 5;
    this._snackbar.open(text, 'OK', {
      duration: durationInSeconds * 1000,
    });
  }

  checkCards() {
    if ( this.cardService.getStatus() == 'sold') {
      this.cards.forEach((element) => {
        if(element.Price == 0) {
          element.Display = true
        } else {
          element.Display = false
        }
      });
    } else {
      this.cards.forEach((element) => {
        if(element.Price == 0) {
          element.Display = false
        } else {
          element.Display = true
        }
      });
    }
  }
}
