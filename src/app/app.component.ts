import { Component } from '@angular/core';
import { HomeComponent } from './home/home.component';
import { CardCollectiblesService } from './services/card-collectibles.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'interview';
  onSale = true;
  
  tabs = [
    {
      label: 'All Collectibles',
      route: 'home'
    },
    {
      label: 'Create',
      route: 'card'
    } 
  ]

  constructor(private cardService: CardCollectiblesService) { }

  // Initialize data
  ngOnInit() {
    this.cardService.loadCollectibles()
  }
}
