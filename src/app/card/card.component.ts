import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { CardCollectiblesService } from '../services/card-collectibles.service';


@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  id: number;
  newCard: boolean;
  label: string;

  infoGroup = new FormGroup({
    name: new FormControl('', Validators.required),
    owner: new FormControl('', Validators.required),
    price: new FormControl('', [
      Validators.required,
      Validators.pattern("^[0-9]*$"),
      Validators.minLength(1),
    ])
  });

  cards: Array<any> = [];
  constructor(private cardService: CardCollectiblesService, private route: ActivatedRoute, private _snackbar: MatSnackBar) { }

  ngOnInit(): void {
    // Get any input/params on the route 
    this.route.params.subscribe(url => {
      this.id = url['id']
    });

    // Check if ID already exists, change the text on the button, 
    // and map the information if the ID already exists
    this.cards = this.cardService.getCardList();
    var item = this.cards.find(item => item.Id == this.id);

    if (item != undefined) {
      this.newCard = false;
      this.label = 'UPDATE COLLECTIBLE';
      this.mapToInput(item);
    } else {
      this.newCard = true;
      this.label = 'CREATE COLLECTIBLE';
    }
  }

  mapToInput(item: any) {
    this.infoGroup.get('name').patchValue(item.Name);
    this.infoGroup.get('owner').patchValue(item.Owner);
    this.infoGroup.get('price').patchValue(item.Price);
  }

  submit() {

    if (!this.infoGroup.valid) {
      this.openSnackBar('Please fill in the form with valid information first.');
      return;
    }

    if (!this.newCard) {
      this.updateCard();
    } else {
      // Generate random ID if user did not input anything in the params
      if (this.id.toString() == '') {
        this.id = Math.floor((Math.random() * 100) + 4);
      }

      var sold = this.infoGroup.value.price == 0 ? true : false;

      this.cardService.addCard({
        Id: this.id,
        Name: this.infoGroup.value.name,
        Owner: this.infoGroup.value.owner,
        Price: Number(this.infoGroup.value.price),
        ImageUrl: './assets/images/sample2.jpeg',
        Display: true,
        isSold: sold
      });
      this.infoGroup.reset();
      this.openSnackBar('Successfully added card collectible!');
    }
  }

  // Update the card info based on the ID given,
  // If user inputted 0 as the price, mark it as sold to prevent it from 
  // appearing on the `For Sale` 
  updateCard() {
    var item = this.cards.find(item => item.Id == this.id);
    
    item['Name'] = this.infoGroup.value.name;
    item['Owner'] = this.infoGroup.value.owner;
    item['Price'] = Number(this.infoGroup.value.price);
    
    this.infoGroup.value.price == 0 ? item['isSold'] = true : item['isSold'] = false;

    this.openSnackBar('Successfully updated the collectible!');
  }

  openSnackBar(text: string) {
    const durationInSeconds = 5;
    this._snackbar.open(text, 'OK', {
      duration: durationInSeconds * 1000
    });
  }
}
